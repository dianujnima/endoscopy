<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class products extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('basic_model');
        //$this->load->model('products');
        $this->load->database();
        $logged_in = $this->session->userdata('is_logged');

        if (!$logged_in) {
            redirect('user');
        }
    }

    public function index() {
        $res['data'] = $this->basic_model->getMultiData("products");
        $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('products',$res);
        $this->load->view('footer');
    }

    function add(){
        if( $this->input->post() ){
            extract($_POST);
            $image_path = $this->basic_model->uploadFile($_FILES['img'],'uploads', array('jpg', 'JPG', 'png', 'PNG', 'gif'));
            if( empty($title) || empty($desc) || empty($image_path) || empty($email) || empty($contact) ){
                echo json_encode(array("error"=>"Please fill all fields"));
            }
            else{
                $arr = array("title"=>$title,"desc"=>$desc,"image"=>$image_path,"contact"=>$contact,"email"=>$email);
                $this->basic_model->insertRecord($arr,"products");
                echo json_encode(array("success"=>"Record inserted successfully","redirect"=>site_url('products')));
            }

        }
        else{
            $this->load->view('header');
            $this->load->view('sidebar');
            $this->load->view('add-product');
            $this->load->view('footer');
        }
    }

    function delete($id){
      if( isset($id) && $id != ''){
        $test = $this->basic_model->deleteRecord('id', $id, 'products');
        echo json_encode(array("success"=>"Product Succesfully Deleted","reload"=>site_url('products')));
      }
    }


}
