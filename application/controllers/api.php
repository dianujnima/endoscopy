<?php

class api extends CI_Controller {

    public function __CONSTRUCT() {

        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('basic_model');
        $this->load->model('usermodel');
        $this->load->database();
    }

    function login() {

          extract($_REQUEST);
            $arr = array();
            if ($this->basic_model->checkParams(array("username", "password", "license_key"))) {
                
            }
            else {
                $encoded_pass = $this->basic_model->encode($password);
                $res = $this->usermodel->login_chk($username, $encoded_pass);
                if ($res) {
                    $new_res = $this->usermodel->chk_license_key($res['id'], $license_key);
                    if (empty($new_res)) {
                        $arr['error'] = "Invalid License Key";
                    } else {
                        $arr = array("username" => $res['username'], "dealer_id" => $res['id'], "license_key_id" => $new_res['id'], "logged_in" => TRUE);
                    }
                } else {
                    $arr['error'] = "Invalid username or password";
                }
            }
            echo json_encode($arr);
        
    }

}
