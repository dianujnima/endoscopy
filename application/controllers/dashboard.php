<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('basic_model');
        $this->load->database();
        $logged_in = $this->session->userdata('is_logged');

        if (!$logged_in) {
            redirect('user');
        }
    }

    public function index() {
        
        $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('dashboard');
        $this->load->view('footer');
    }
    
    
}
