<?php

    class user extends CI_Controller{
       public $user_id;
       public $current_date;
       public $logged_in;
        public function __CONSTRUCT(){

            parent::__construct();
            $this->load->helper(array('form', 'url'));
            $this->load->library('form_validation');
            $this->load->library('session');
            $this->load->model('usermodel');
            $this->load->model('basic_model');
            $this->load->database();
            $logged_in = $this->session->userdata('is_logged');
            $this->user_id = $this->session->userdata('id');
            $this->current_date = date("Y-m-d h:i:s A");


        }

        public function index(){

           $logged_in = $this->session->userdata('is_logged');
            if( $logged_in ){

                redirect('distributors','refresh');
            }
            else{
            $this->load->view("login");
            }

        }


        function login(){

           if( $this->input->post() ){
           extract($_POST);

             $pass = $this->basic_model->encode($password);
             $res = $this->usermodel->chk_login("admin",$username,$pass);
                if( $res ){
                     $sess_data = array("id"=>$res['id'],"username"=>$res['username'],"password"=>$password,"is_logged"=>1);
                     $this->session->set_userdata($sess_data);
                     echo json_encode(array("success"=>"Logged in","redirect"=>site_url('distributors')));
                }
                else{
                    echo json_encode(array("error"=>"Invalid username or password"));
                }
           }

        }

        function logout(){

            $arr = array("username"=>"","password"=>"","is_logged"=>0);
            $this->session->unset_userdata($arr);
            $this->session->sess_destroy();
            redirect('user','refresh');
        }
    }
