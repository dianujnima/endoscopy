<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class distributors extends CI_Controller {
    public $email;
    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('basic_model');
        $this->load->model('common_model');
        $this->load->database();
        $logged_in = $this->session->userdata('is_logged');
        $this->email = $this->session->userdata('email');
        if (!$logged_in) {
            redirect('user');
        }
    }

    public function index() {
        
        $res['data'] = $this->common_model->getDistributors();
        $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('distributors',$res);
        $this->load->view('footer');
    }
    
    function add(){
        if( $this->input->post() ){
            extract($_POST);
            if( empty($fname) || empty($lname) || empty($username) || empty($password) || empty($email) || empty($contact) || empty($address) || empty($country) || empty($city) || empty($state) || empty($zip) ){
                 echo json_encode(array("error"=>"Please fill all required fields"));
            }
            else{
                $encoded_pass = $this->basic_model->encode($password);
                $res = $this->common_model->getLastDealer();
                $dealer_id = str_pad(1, 3, '0', STR_PAD_LEFT);
                if( $res ){
                    $id = $res['id'];
                    $id++;
                    $dealer_id = str_pad($id, 3, '0', STR_PAD_LEFT);
                }
                $arr = array("id"=>$dealer_id,"fname"=>$fname,"lname"=>$lname,"username"=>$username,"password"=>$encoded_pass,"contact"=>$contact,"email"=>$email,"address"=>$address,"country"=>$country,"city"=>$city,"state"=>$state,"zip"=>$zip);
                $this->basic_model->insertRecord($arr,"distributors");
                //code to generate license keys
                $new_arr = $this->basic_model->getRow("country","id",$country);
                $country_short = substr($new_arr['country'],0,3);
                $data = $this->licenseKeyGenerator($total,$dealer_id,$country_short);
                foreach( $data as $k=>$v ){ 
                    $arr = array("distributor_id"=>$dealer_id,"license_key"=>$v,"status"=>"active");
                    $this->basic_model->insertRecord($arr,"license_keys");
                }
                //email code
                 
                $to=$email;
		$subj="Account Details";
		$from = $this->email;
		$msg='<html>
		  <body style="font-family:Arial;font-size:14px;">
		   
		        <p>Dear <b>'.$fname.' '.$lname. '</b></p>
                        <p></p>
				<p>Username:<strong>'.$username.'</strong></p>
				<p></p>
				<p>Password:<strong>'.$password.'</strong></p>
                                <p>Your License Keys:</p>
                                <p> 
                            <ul>
		        ';
                        foreach( $data as $k=>$v ){
		        	$msg.='<li><strong>'.$v.'</strong></li>';
		        }
		 	$msg.='</ul>
			</p>
                        </div>		
			
		  </body>
		</html>';
                $this->basic_model->sendEmail($from,$to,$msg,$subj);
                echo json_encode(array("success"=>"Record added successfully","redirect"=>site_url('distributors')));
            }
        }
        else{
            $res['country'] = $this->basic_model->getMultiData("country");
            $this->load->view('header');
            $this->load->view('sidebar');
            $this->load->view('add-distributor',$res);
            $this->load->view('footer');
        }
    }
    
    
       function licenseKeyGenerator($number,$id,$country){
           
         $new_arr = array();
         $start_part = $country."-".$id."-";
         $res = $this->common_model->getSpecificLicenseKeys($start_part);
         for($j=1;$j<=$number;$j++){    
          $serial = '';
          $temp = $j;
          $chars = array(0,1,2,3,4,5,6,7,8,9,'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
          $max = count($chars)-1;
          for($i=0;$i<9;$i++){
              $serial .= (!($i % 3) && $i ? '-' : '').$chars[rand(0, $max)];
          }
          $license_key = $start_part.$serial;
          if( $res ){
              foreach( $res as $key=>$v ){
                  if( in_array($license_key,$res) ){
                      $j=$temp;
                  }
                  else{
                      $new_arr[] = $license_key;
                  }
              }
          }
          else{
              $new_arr[] = $license_key;
          }
            
         }
         return $new_arr;
        }
    
    function generateLicenseKeys(){
        
        if( $this->input->post() ){
            extract($_POST);
           $res = $this->common_model->getDistributors($dealer_id);   
           $country = $res[0]['country'];
           $email = $res[0]['email'];
           $country_short = substr($country,0,3);
           $data = $this->licenseKeyGenerator($total,$dealer_id,$country_short);
           foreach( $data as $k=>$v ){
               $arr = array("distributor_id"=>$dealer_id,"license_key"=>$v,"status"=>"active");
               $this->basic_model->insertRecord($arr,"license_keys");
           }
            //email code
                 
                $to=$email;
		$subj="License Keys";
		$from = $this->email;
		$msg='<html>
		  <body style="font-family:Arial;font-size:14px;">
		    <p></p>
			<p>Your License Keys:</p>
                        <p> 
                        <ul>
		        ';
                        foreach( $data as $k=>$v ){
		        	$msg.='<li><strong>'.$v.'</strong></li>';
		        }
		 	$msg.='</ul>
			</p>
                        </div>		
			
		  </body>
		</html>';
                $this->basic_model->sendEmail($from,$to,$msg,$subj);
           echo json_encode(array("success"=>"Record added successfully","redirect"=>site_url('distributors/licenseKeys')));
           
        }
        else{
            $res['data'] = $this->basic_model->getMultiData("distributors");
            $this->load->view('header');
            $this->load->view('sidebar');
            $this->load->view('generate-license-keys',$res);
            $this->load->view('footer');  
        }
        
        
    }
    
    function licenseKeys(){
        
        $res['data'] = $this->common_model->getLicenseKeys();
        $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('view-license-keys',$res);
        $this->load->view('footer');
    }
    
    function getSpecificLicenseKeys(){
        $id = $_POST['dealer_id'];
        $res['data'] = $this->common_model->getDealersLicenseKeys($id);
        $this->load->view('ajax-license-key-view',$res);
    }
    
}
