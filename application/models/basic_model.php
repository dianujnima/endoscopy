<?php

class Basic_model extends CI_Model {

	
public $countries = array('AF' => 'Afghanistan','AX' => 'Aland Islands','AL' => 'Albania','DZ' => 'Algeria','AS' => 'American Samoa','AD' => 'Andorra','AO' => 'Angola','AI' => 'Anguilla','AQ' => 'Antarctica','AG' => 'Antigua And Barbuda','AR' => 'Argentina','AM' => 'Armenia','AW' => 'Aruba','AU' => 'Australia','AT' => 'Austria','AZ' => 'Azerbaijan','BS' => 'Bahamas','BH' => 'Bahrain','BD' => 'Bangladesh','BB' => 'Barbados','BY' => 'Belarus','BE' => 'Belgium','BZ' => 'Belize','BJ' => 'Benin','BM' => 'Bermuda','BT' => 'Bhutan','BO' => 'Bolivia','BA' => 'Bosnia And Herzegovina','BW' => 'Botswana','BV' => 'Bouvet Island','BR' => 'Brazil','IO' => 'British Indian Ocean Territory','BN' => 'Brunei Darussalam','BG' => 'Bulgaria','BF' => 'Burkina Faso','BI' => 'Burundi','KH' => 'Cambodia','CM' => 'Cameroon','CA' => 'Canada','CV' => 'Cape Verde','KY' => 'Cayman Islands','CF' => 'Central African Republic','TD' => 'Chad','CL' => 'Chile','CN' => 'China','CX' => 'Christmas Island','CC' => 'Cocos (Keeling) Islands','CO' => 'Colombia','KM' => 'Comoros','CG' => 'Congo','CD' => 'Congo, Democratic Republic','CK' => 'Cook Islands','CR' => 'Costa Rica','CI' => 'Cote D\'Ivoire','C_I' => 'Canary Island','HR' => 'Croatia','CU' => 'Cuba','CY' => 'Cyprus','CZ' => 'Czech Republic','DK' => 'Denmark','DJ' => 'Djibouti','DM' => 'Dominica','DO' => 'Dominican Republic','EC' => 'Ecuador','EG' => 'Egypt','SV' => 'El Salvador','GQ' => 'Equatorial Guinea','ER' => 'Eritrea','EE' => 'Estonia','ET' => 'Ethiopia','FK' => 'Falkland Islands (Malvinas)','FO' => 'Faroe Islands','FJ' => 'Fiji','FI' => 'Finland','FR' => 'France','GF' => 'French Guiana','PF' => 'French Polynesia','TF' => 'French Southern Territories','GA' => 'Gabon','GM' => 'Gambia','GE' => 'Georgia','DE' => 'Germany','GH' => 'Ghana','GI' => 'Gibraltar','GR' => 'Greece','GL' => 'Greenland','GD' => 'Grenada','GP' => 'Guadeloupe','GU' => 'Guam','GT' => 'Guatemala','GG' => 'Guernsey','GN' => 'Guinea','GW' => 'Guinea-Bissau','GY' => 'Guyana','HT' => 'Haiti','HM' => 'Heard Island & Mcdonald Islands','VA' => 'Holy See (Vatican City State)','HN' => 'Honduras','HK' => 'Hong Kong','HU' => 'Hungary','IS' => 'Iceland','IN' => 'India','ID' => 'Indonesia','IR' => 'Iran, Islamic Republic Of','IQ' => 'Iraq','IE' => 'Ireland','IM' => 'Isle Of Man','IL' => 'Israel','IT' => 'Italy','JM' => 'Jamaica','JP' => 'Japan','JE' => 'Jersey','JO' => 'Jordan','KZ' => 'Kazakhstan','KE' => 'Kenya','KI' => 'Kiribati','KR' => 'Korea','KW' => 'Kuwait','KG' => 'Kyrgyzstan','LA' => 'Lao People\'s Democratic Republic','LV' => 'Latvia','LB' => 'Lebanon','LS' => 'Lesotho','LR' => 'Liberia','LY' => 'Libyan Arab Jamahiriya','LI' => 'Liechtenstein','LT' => 'Lithuania','LU' => 'Luxembourg','MO' => 'Macao','MK' => 'Macedonia','MG' => 'Madagascar','MW' => 'Malawi','MY' => 'Malaysia','MV' => 'Maldives','ML' => 'Mali','MT' => 'Malta','MH' => 'Marshall Islands','MQ' => 'Martinique','MR' => 'Mauritania','MU' => 'Mauritius','YT' => 'Mayotte','MX' => 'Mexico','FM' => 'Micronesia, Federated States Of','MD' => 'Moldova','MC' => 'Monaco','MN' => 'Mongolia','ME' => 'Montenegro','MS' => 'Montserrat','MA' => 'Morocco','MZ' => 'Mozambique','MM' => 'Myanmar','NA' => 'Namibia','NR' => 'Nauru','NP' => 'Nepal','NL' => 'Netherlands','AN' => 'Netherlands Antilles','NC' => 'New Caledonia','NZ' => 'New Zealand','NIR'=>'Northern Ireland','NI' => 'Nicaragua','NE' => 'Niger','NG' => 'Nigeria','NU' => 'Niue','NF' => 'Norfolk Island','MP' => 'Northern Mariana Islands','NO' => 'Norway','OM' => 'Oman','PK' => 'Pakistan','PW' => 'Palau','PS' => 'Palestinian Territory, Occupied','PA' => 'Panama','PG' => 'Papua New Guinea','PY' => 'Paraguay','PE' => 'Peru','PH' => 'Philippines','PN' => 'Pitcairn','PL' => 'Poland','PT' => 'Portugal','PR' => 'Puerto Rico','QA' => 'Qatar','RE' => 'Reunion','RO' => 'Romania','RU' => 'Russian Federation','RW' => 'Rwanda','BL' => 'Saint Barthelemy','SH' => 'Saint Helena','KN' => 'Saint Kitts And Nevis','LC' => 'Saint Lucia','MF' => 'Saint Martin','PM' => 'Saint Pierre And Miquelon','VC' => 'Saint Vincent And Grenadines','WS' => 'Samoa','SM' => 'San Marino','ST' => 'Sao Tome And Principe','SA' => 'Saudi Arabia','SN' => 'Senegal','RS' => 'Serbia','SC' => 'Seychelles','SL' => 'Sierra Leone','SG' => 'Singapore','SK' => 'Slovakia','SI' => 'Slovenia','SB' => 'Solomon Islands','SO' => 'Somalia','ZA' => 'South Africa','GS' => 'South Georgia And Sandwich Isl.','ES' => 'Spain','LK' => 'Sri Lanka','SD' => 'Sudan','SR' => 'Suriname','SJ' => 'Svalbard And Jan Mayen','SZ' => 'Swaziland','SE' => 'Sweden','CH' => 'Switzerland','SY' => 'Syrian Arab Republic','TW' => 'Taiwan','TJ' => 'Tajikistan','TZ' => 'Tanzania','TH' => 'Thailand','TL' => 'Timor-Leste','TG' => 'Togo','TK' => 'Tokelau','TO' => 'Tonga','TT' => 'Trinidad And Tobago','TN' => 'Tunisia','TR' => 'Turkey','TM' => 'Turkmenistan','TC' => 'Turks And Caicos Islands','TV' => 'Tuvalu','UG' => 'Uganda','UA' => 'Ukraine','AE' => 'United Arab Emirates','GB' => 'United Kingdom','US' => 'United States','UM' => 'United States Outlying Islands','UY' => 'Uruguay','UZ' => 'Uzbekistan','VU' => 'Vanuatu','VE' => 'Venezuela','VN' => 'Viet Nam','VG' => 'Virgin Islands, British','VI' => 'Virgin Islands, U.S.','WF' => 'Wallis And Futuna','EH' => 'Western Sahara','YE' => 'Yemen','ZM' => 'Zambia','ZW' => 'Zimbabwe');
   

	

public $miscellaneous=array('Google','Yellow Pages','Radio','Newspaper','Megazine Advert','Recommended By friend','Esisting Customer','Other');





    function insertRecord($arr, $tablename) {

        $arr_length = count($arr);

        if ($arr_length > 0) {

            $q = "INSERT INTO `" . $tablename . "`";

            $str_column = "";

            $str_val = "";

            $i = 1;

            foreach ($arr as $key => $value) {

                if ($i == 1) {

                    $str_column.="(";

                    $str_val.=" VALUES(";

                }

                $str_column.="`" . $key . "`,";

                $str_val.=$this->db->escape($value) . ",";

                if ($i == $arr_length) {

                    $str_column = substr($str_column, 0, -1);

                    $str_val = substr($str_val, 0, -1);

                    $str_column.=")";

                    $str_val.=")";

                }

                $i++;

            }

            $q = $q . $str_column . $str_val;

            $this->db->query($q);

            $id = mysql_insert_id();

            return $id;

        }

    }



    function updateRecord($arr, $tablename, $column, $val) {

        $arr_length = count($arr);

        if ($arr_length > 0) {

            $q = "UPDATE `" . $tablename . "` SET";

            $str_column = "";

            $i = 1;

            foreach ($arr as $key => $value) {

                $str_column.=" `" . $key . "`=" . "" . $this->db->escape($value) . ",";

                if ($i == $arr_length) {

                    $str_column = substr($str_column, 0, -1);

                    $str_column.=" WHERE `" . $column . "`='" . $val . "'";

                }

                $i++;

            }

            $q = $q . $str_column;

            $this->db->query($q);

        }

    }



    function getRows($table_name, $column, $criteria) {

        $query = "select  * from  `" . $table_name . "` where `" . $column . "`=" . $this->db->escape($criteria)." order by ".$column." DESC";

        $result = $this->db->query($query);

        return $result->result_array();

    }



    function getMultiData($table_name,$column='id', $order = "ASC") {

        $query = "select  * from  `" . $table_name . "`";

        $query.=" ORDER BY `".$column."` " . $order;

        $result = $this->db->query($query);

        return $result->result_array();

    }

    function getRow($table_name, $column, $criteria, $order = "ASC") {

        $query = "select  * from  `" . $table_name . "` where `" . $column . "`=" . $this->db->escape($criteria) . " ORDER BY `id` " . $order;

        $result = $this->db->query($query);

        if ($result->num_rows() > 0) {

            return $result->row_array();

        }

    }



    function deleteRecord($column, $id, $tablename) {

        $query = "DELETE FROM `" . $tablename . "` WHERE `" . $column . "`=" . $this->db->escape($id) . "";

        $result = $this->db->query($query);

        return $result;

    }



    function selectSelect($sel_val, $array) {

        $data = "";

        foreach ($array as $key => $value) {

            $active = "";

            if ($sel_val == $value) {

                $active = "selected='selected'";

            }

            if ($active == "" && $key == $sel_val) {

                $active = "selected='selected'";

            }

            $data.="<option value='" . $key . "' " . $active . ">" . $value . "</option>";

        }

        return $data;

    }



    function selectSelect2($sel_val, $array, $name, $id, $val_only = false) {

        $data = "";

        foreach ($array as $key => $value) {

            if ($val_only) {

                if ($sel_val == $value[$id]) {

                    $data.="<span>" . $value[$name] . "</span>";

                }

            } else {

                $active = "";

                if ($sel_val == $value[$id]) {

                    $active = "selected='selected'";

                }

                $data.="<option value='" . $value[$id] . "' " . $active . ">" . $value[$name] . "</option>";

            }

        }

        return $data;

    }



    function getCustomRows($query) {

        $result = $this->db->query($query);

        return $result->result_array();

    }



    function getCustomRow($query) {

        $result = $this->db->query($query);

        if ($result->num_rows() > 0) {

            return $result->row_array();

        }

    }



    function encode($name) {

        for ($a = 0; $a < 2; $a++) {

            $name = base64_encode($name);



            for ($b = 0; $b < 3; $b++) {

                $name = base64_encode($name);

            }

        }

        return $name;

    }



    function decode($name) {

        for ($a = 0; $a < 2; $a++) {

            $name = base64_decode($name);



            for ($b = 0; $b < 3; $b++) {

                $name = base64_decode($name);

            }

        }

        return $name;

    }



    function pagination($query, $count_query, $targetpages, $pgs, $orderby, $order,$limit) {

        // How many adjacent pagess should be shown on each side?

        $adjacents = 3;



        /*

          First get total number of rows in data table.

          If you have a WHERE clause in your query, make sure you mirror it here.

         */

        //$query = "SELECT COUNT(*) FROM $tbl_name";

        $total_pages = $this->getCustomRow($count_query);

        $total_pages = $total_pages['total'];



        /* Setup vars for query. */

        if ($pgs == "all") {

            $limit = 30000;

            $pages = 1;

        } else {

            $limit = $limit;         //how many items to show per pages

            $pages = isset($pgs) ? $pgs : 1;

        }

        if ($pages)

            $start = ($pages - 1) * $limit;    //first item to display on this pages

        else

            $start = 0;        //if no pages var is given, set start to 0



            /* Get data. */

        $sql = $query . " order by " . $orderby . " " . $order . " LIMIT $start, $limit";

        $result = $this->getCustomRows($sql);



        /* Setup pages vars for display. */

        if ($pages == 0)

            $pages = 1;     //if no pages var is given, default to 1.

        $prev = $pages - 1;       //previous pages is pages - 1

        $next = $pages + 1;       //next pages is pages + 1

        $lastpages = ceil($total_pages / $limit);  //lastpages is = total pagess / items per pages, rounded up.

        $lpm1 = $lastpages - 1;      //last pages minus 1



        /*

          Now we apply our rules and draw the pagination object.

          We're actually saving the code to a variable in case we want to draw it more than once.

         */

        $pagination = "";

        if ($lastpages > 1) {

            $pagination .= "<div class=\"center\"><ul class=\"pagination\">";
            //previous button

            if ($pages > 1)

                $pagination.= "<li><a href=\"" . site_url(array($targetpages)) . "/$prev\">< previous</a></li>";

            else

                $pagination.= "<li><span class=\"disabled\">< previous</span></li>";



            //pagess

            if ($lastpages < 7 + ($adjacents * 2)) { //not enough pagess to bother breaking it up

                for ($counter = 1; $counter <= $lastpages; $counter++) {

                    if ($counter == $pages)

                        $pagination.= "<li><span class=\"current\">$counter</span></li>";

                    else

                        $pagination.= "<li><a href=\"" . site_url(array($targetpages)) . "/$counter\">$counter</a></li>";

                }

            }

            elseif ($lastpages > 5 + ($adjacents * 2)) { //enough pagess to hide some

                //close to beginning; only hide later pagess

                if ($pages < 1 + ($adjacents * 2)) {

                    for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {

                        if ($counter == $pages)

                            $pagination.= "<li><span class=\"current\">$counter</span></li>";

                        else

                            $pagination.= "<li><a href=\"" . site_url(array($targetpages)) . "/$counter\">$counter</a></li>";

                    }

                    $pagination.= "...";

                    $pagination.= "<li><a href=\"" . site_url(array($targetpages)) . "/$lpm1\">$lpm1</a></li>";

                    $pagination.= "<li><a href=\"" . site_url(array($targetpages)) . "/$lastpages\">$lastpages</a></li>";

                }

                //in middle; hide some front and some back

                elseif ($lastpages - ($adjacents * 2) > $pages && $pages > ($adjacents * 2)) {

                    $pagination.= "<li><a href=\"&pages=1\">1</a></li>";

                    $pagination.= "<li><a href=\"&pages=2\">2</a></li>";

                    $pagination.= "...";

                    for ($counter = $pages - $adjacents; $counter <= $pages + $adjacents; $counter++) {

                        if ($counter == $pages)

                            $pagination.= "<li><span class=\"current\">$counter</span></li>";

                        else

                            $pagination.= "<li><a href=\"" . site_url(array($targetpages)) . "/$counter\">$counter</a></li>";

                    }

                    $pagination.= "...";

                    $pagination.= "<a href=\"" . site_url(array($targetpages)) . "/$lpm1\">$lpm1</a>";

                    $pagination.= "<a href=\"" . site_url(array($targetpages)) . "/$lastpages\">$lastpages</a>";

                }

                //close to end; only hide early pagess

                else {

                    $pagination.= "<a href=\"" . site_url(array($targetpages)) . "/1\">1</a>";

                    $pagination.= "<a href=\"" . site_url(array($targetpages)) . "/2\">2</a>";

                    $pagination.= "...";

                    for ($counter = $lastpages - (2 + ($adjacents * 2)); $counter <= $lastpages; $counter++) {

                        if ($counter == $pages)

                            $pagination.= "<li><span class=\"current\">$counter</span></li>";

                        else

                            $pagination.= "<li><a href=\"" . site_url(array($targetpages)) . "/$counter\">$counter</a></li>";

                    }

                }

            }



            //next button

            if ($pages < $counter - 1)

                $pagination.= "<li><a href=\"" . site_url(array($targetpages)) . "/$next\">next ></a></li>";

            else

                $pagination.= "<li><span class=\"disabled\">next ></span></li>";

            $pagination.= "<li><a href=\"" . site_url(array($targetpages)) . "/all\">View All</a></li></ul></div>\n";

        }

        $c = array($pagination, $result);

        return $c;

    }



    function urlEncrypt($str) {

        $key = "abc123 as long as you want bla bla bla";

        $result = '';

        for ($i = 0; $i < strlen($str); $i++) {

            $char = substr($str, $i, 1);

            $keychar = substr($key, ($i % strlen($key)) - 1, 1);

            $char = chr(ord($char) + ord($keychar));

            $result.=$char;

        }

        return urlencode(base64_encode($result));

    }



    function urlDecrypt($str) {

        $str = base64_decode(urldecode($str));

        $result = '';

        $key = "abc123 as long as you want bla bla bla";

        for ($i = 0; $i < strlen($str); $i++) {

            $char = substr($str, $i, 1);

            $keychar = substr($key, ($i % strlen($key)) - 1, 1);

            $char = chr(ord($char) - ord($keychar));

            $result.=$char;

        }

        return $result;

    }



    function sendEmail($from, $to, $body, $subject, $from_name = NULL) {
        if ($from_name == "") {
            $from_name = $from;
        }
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: ' . $from . ' <' . $from . '>' . "\r\n";
        $res = mail($to, $subject, $body, $headers);
        return $res;
    }



    function multiSearch($array, $key, $value) {

        $results = array();



        if (is_array($array)) {

            if (isset($array[$key]) && $array[$key] == $value)

                $results[] = $array;



            foreach ($array as $subarray)

                $results = array_merge($results, $this->multiSearch($subarray, $key, $value));

        }



        return $results;

    }



    function multiSelect2($sel_val, $array, $name, $id, $findname) {

        $data = "";



        foreach ($array as $key => $value) {

            $active = "";

            $res = $this->multiSearch($sel_val, $findname, intval($value[$id]));

            if (count($res) > 0) {

                $active = "selected='selected'";

            }

            $data.="<option value='" . $value[$id] . "' " . $active . ">" . $value[$name] . "</option>";

        }



        return $data;

    }



    function multiSelectValue($sel_vals, $array, $name, $id, $val_only = false) {

        $data = "";

        foreach ($array as $key => $value) {

            if ($val_only) {

                if (in_array($value[$id], $sel_vals)) {

                    $data.="<span>" . $value[$name] . "</span>, ";

                }

            } else {

                $active = "";

                if (in_array($value[$id], $sel_vals)) {

                    $active = "selected='selected'";

                }

                $data.="<option value='" . $value[$id] . "' " . $active . ">" . $value[$name] . "</option>";

            }

        }

        return $data;

    }



    function multiSelectValue2($sel_vals, $array, $val_only = false) {

        $data = "";

        foreach ($array as $key => $value) {

            if ($val_only) {

                if (in_array($key, $sel_vals)) {

                    $data.="<span>" . $value . "</span>";

                }

            } else {

                $active = "";

                if (in_array($key, $sel_vals)) {

                    $active = "selected='selected'";

                }

                $data.="<option value='" . $key . "' " . $active . ">" . $value . "</option>";

            }

        }

        return $data;

    }



    function is_login($obj) {

        $logged_in = $obj->session->userdata('logged_in');

        if (!$logged_in) {

            redirect("user");

            return;

        } else {

            $user_id = $obj->session->userdata('user_id');

            return $user_id;

        }

    }



    function uploadFile($file, $uploads_dir, $allowfiles) {

        if ($file['size'] > 0) {

            $pic_name = "";

            $ext = pathinfo($file['name'], PATHINFO_EXTENSION);

            if (in_array($ext, $allowfiles)) {

                $pic_name = rand();

                $tmp_name = $file["tmp_name"];

                $pic_name.= $file["name"];

                move_uploaded_file($tmp_name, "$uploads_dir/$pic_name");

                return $pic_name;

            } else {

                return false;

                ;

            }

        } else {

            return "";

        }

    }



    function Email($remail, $rname, $semail, $sname, $cc, $bcc, $subject, $message, $attachments, $priority, $type) {

        $fullmessage = "";

        // Checks if carbon copy & blind carbon copy exist

        if ($cc != null) {

            $cc = "CC: " . $cc . "\r\n";

        } else {

            $cc = "";

        }

        if ($bcc != null) {

            $bcc = "BCC: " . $bcc . "\r\n";

        } else {

            $bcc = "";

        }



        // Checks the importance of the email

        if ($priority == "high") {

            $priority = "X-Priority: 1\r\nX-MSMail-Priority: High\r\nImportance: High\r\n";

        } elseif ($priority == "low") {

            $priority = "X-Priority: 3\r\nX-MSMail-Priority: Low\r\nImportance: Low\r\n";

        } else {

            $priority = "";

        }



        // Checks if it is plain text or HTML

        if ($type == "plain") {

            $type = "text/plain";

        } else {

            $type = "text/html";

        }



        // The boundary is set up to separate the segments of the MIME email

        $boundary = md5(@date("Y-m-d-g:ia"));



        // The header includes most of the message details, such as from, cc, bcc, priority etc.

        $header = "From: " . $sname . " <" . $semail . ">\r\nMIME-Version: 1.0\r\nX-Mailer: PHP\r\nReply-To: " . $sname . " <" . $semail . ">\r\nReturn-Path: " . $sname . " <" . $semail . ">\r\n" . $cc . $bcc . $priority . "Content-Type: multipart/mixed; boundary = " . $boundary . "\r\n\r\n";



        // The full message takes the message and turns it into base 64, this basically makes it readable at the recipients end

        $fullmessage .= "--" . $boundary . "\r\nContent-Type: " . $type . "; charset=UTF-8\r\nContent-Transfer-Encoding: base64\r\n\r\n" . chunk_split(base64_encode($message));



        // A loop is set up for the attachments to be included.



        if ($attachments != null) {

            foreach ($attachments as $attachment) {

                $attachment = explode("#", $attachment);

                $fullmessage .= "--" . $boundary . "\r\nContent-Type: " . $attachment[1] . "; name=\"" . $attachment[2] . "\"\r\nContent-Transfer-Encoding: base64\r\nContent-Disposition: attachment\r\n\r\n" . chunk_split(base64_encode(file_get_contents($attachment[0])));

            }

        }

        // And finally the end boundary to set the end of the message

        $fullmessage .= "--" . $boundary . "--";

        return mail($rname . "<" . $remail . ">", $subject, $fullmessage, $header);

    }



    function contact($data) {

        extract($data);

        $from = $email;

        $to = "admin@serpcloud.com";

        $subject = 'Contact From SerpCloud';

        $body = "

            <table>

                <tr>

                    <td>Name : </td> <td>" . $name . "</td>

                </tr>

                <tr>

                    <td>Email : </td> <td>" . $email . "</td>

                </tr>

                <tr>

                    <td>Subject : </td> <td>" . $subject . "</td>

                </tr>

                <tr>

                    <td>Message : </td> <td>" . $message . "</td>

                </tr>

            </table>";

        $this->sendEmail($from, $to, $body, $subject);

    }



    function customSmtp($body, $subject, $to, $result, $from, $from_n) {

        $this->phpmailer->IsSMTP(); // telling the class to use SMTP

        $this->phpmailer->Host = "mail.serpcloud.com.com"; // SMTP server

        $this->phpmailer->SMTPDebug = 0;                     // enables SMTP debug information (for testing)

        $this->phpmailer->SMTPAuth = true;                  // enable SMTP authentication

        $this->phpmailer->SMTPSecure = $result['smtp_secure'];                 // sets the prefix to the servier

        $this->phpmailer->Host = $result['host'];      // sets GMAIL as the SMTP server

        $this->phpmailer->Port = $result['port'];                   // set the SMTP port for the GMAIL server

        $this->phpmailer->Username = $result['username'];  // GMAIL username

        $this->phpmailer->Password = $this->decode($result['password']);            // GMAIL password

        //$this->phpmailer->SetFrom($result['from_email'], $result['from_name']);



        $this->phpmailer->SetFrom($from, $from_n);



        $this->phpmailer->AddReplyTo($result['repy_email'], $result['reply_name']);



        $this->phpmailer->Subject = $subject;



        $this->phpmailer->AltBody = "Serp Cloud"; // optional, comment out and test

        if (@$_FILES['file']['size'] > 0) {

            $this->phpmailer->Body($body);

            $this->phpmailer->AddAttachment($_FILES['file']['tmp_name'], $_FILES['file']['name']);

        } else {

            $this->phpmailer->MsgHTML($body);

        }

        $address = $to;

        $this->phpmailer->AddAddress($address);

        if (!$this->phpmailer->Send()) {

            //echo "Mailer Error: " . $this->phpmailer->ErrorInfo;

            return false;

        } else {

            //echo "Message sent!";

            return true;

        }

    }



    function checkAccountStatus() {

        $id = $this->session->userdata("user_id");

        $role = $this->session->userdata("role");

        if ($role) {

            $result = $this->basic_model->getRow("users", "id", $id);

            //var_dump($result);

//            /exit;

            if ($result['status']) {

                $this->logout_account();

            }

        } else {

            $result = $this->basic_model->getRow("addon_users", "id", $id);

            if (!is_array($result)) {

                $this->logout_account();

            }

        }

    }



    function logout_account() {

        $newdata = array(

            'email',

            'user_id',

            'logged_in' => FALSE,

            'name',

            'role',

            'profile_image'

        );

        $this->session->unset_userdata($newdata);

        //$this->session->sess_destroy();

        redirect('user', 'refresh');

    }





    function RelativeTime($timestamp) {

        $difference = time() - $timestamp; // Make different between this time and time value which pass throw $timestamp

        $periods = array("sec", "min", "hour", "day", "week", "month", "years", "decade");

        $lengths = array("60", "60", "24", "7", "4.35", "12", "10");

        if ($difference > 0) { // this was in the past 

            $ending = "ago";

        } else { // this was in the future

            $difference = -$difference;

            $ending = "to go";

        }



        for ($j = 0; $difference >= $lengths[$j]; $j++)

            $difference /= $lengths[$j];



        $difference = round($difference);



        if ($difference != 1)

            $periods[$j].= "s";



        $text = "$difference $periods[$j] $ending";

        if ($text == "0 secs to go") {

            $text = "few sec ago";

        }

        return $text;

    }

    

    function truncate($table=NULL){

        $sql = "TRUNCATE `".$this->db->escape_str($table)."`";

        $this->db->query($sql);

    }
	
	
	 function sendEmailSimple($from, $to, $body, $subject, $from_name = "") {
        if ($from_name == "") {
            $from_name = $from;
        }
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: ' . $from . ' <' . $from . '>' . "\r\n";
        $res = mail($to, $subject, $body, $headers);
        return $res;
    }
    
     function checkParams($array = NULL) {
        if (!$array) {
            echo json_encode(array("status" => "Error", "msg" => "Parameters Required"));
            exit;
        } else {
            $check = FALSE;
            $param_name = "";
            foreach ($array as $key => $value) {
                if (!array_key_exists($value, $_REQUEST) && trim(@$_REQUEST[$value]) == "") {
                    $param_name = $value;
                    $check = TRUE;
                }
            }
            if ($check) {
                echo json_encode(array("status" => "Error", "msg" => $param_name . " parameter is missing OR missing value"));
                exit;
            }
        }
    }


}

?>
