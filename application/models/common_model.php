<?php

class common_model extends CI_Model{

    function getDistributors($id=0){
        $q = '';
        if( $id != "" ){
            $q = 'WHERE d.id = '.$id.'  ';
        }
        $sql = 'SELECT d.*,c.country from distributors d JOIN country c ON c.id = d.country '.$q.' ';
        $res = $this->db->query($sql);
        return $res->result_array();
    }

    function getLastDealer(){
        $sql = 'SELECT id from distributors ORDER BY id DESC limit 1';
        $res = $this->db->query($sql);
        return $res->row_array();
    }

    function getSpecificLicenseKeys($key){
        $sql ='SELECT license_key from license_keys WHERE id like "%'.$key.'%"';
        $res = $this->db->query($sql);
        return $res->result_array();
    }

    function getLicenseKeys(){
        // $sql = 'SELECT d.id,CONCAT(fname," ",lname) `name`,COUNT(l.license_key) `total` FROM license_keys l
        //         JOIN distributors d ON d.id =l.distributor_id
        //         GROUP BY l.distributor_id';
        $sql = 'SELECT d.id,CONCAT(fname," ",lname) `name`,COUNT(l.license_key) `total`,
                COUNT(CASE WHEN l.`status` = "active" THEN 1 ELSE NULL END) `active`,
                COUNT(CASE WHEN l.`status` = "used" THEN 1 ELSE NULL END) `deactive`
                FROM license_keys l
                JOIN distributors d ON d.id =l.distributor_id
                GROUP BY l.distributor_id  ORDER BY d.id DESC';

        $res = $this->db->query($sql);
        return $res->result_array();
    }

    function getDealersLicenseKeys($distributor_id){
        // $sql = 'SELECT k.license_key `dealer_key`,d.id,u.license_key FROM license_keys k
        //         JOIN distributors d ON d.id = k.distributor_id
        //         LEFT JOIN users u ON u.license_key = k.id
        //         WHERE k.distributor_id = '.$distributor_id.' ';

        $sql = 'SELECT k.id,k.license_key `dealer_key`,d.id, k.status FROM license_keys k
                JOIN distributors d ON d.id = k.distributor_id
                WHERE k.distributor_id = ' .$distributor_id.
                ' ORDER BY k.id DESC';

        $res = $this->db->query($sql);
        return $res->result_array();
    }
}
