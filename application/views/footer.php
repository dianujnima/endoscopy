<footer role="contentinfo">
    <div class="clearfix">
        <ul class="list-unstyled list-inline pull-left">
            <li><h6 style="margin: 0;"> &copy; 2016 Gold Star Medical</h6></li>
        </ul>
        <button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top"><i class="fa fa-arrow-up"></i></button>
    </div>
</footer>
                </div>
            </div>
        </div>
<script src="<?php echo base_url()?>assets/js/jquery-1.10.2.min.js"></script> 							<!-- Load jQuery -->
<script src="<?php echo base_url()?>assets/js/jqueryui-1.9.2.min.js"></script> 							<!-- Load jQueryUI -->

<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script> 								<!-- Load Bootstrap -->

<script src="<?php echo base_url()?>assets/js/enquire.min.js"></script> 									<!-- Enquire for Responsiveness -->

<script src="<?php echo base_url()?>assets/plugins/nanoScroller/js/jquery.nanoscroller.min.js"></script> <!-- nano scroller -->

<script src="<?php echo base_url()?>assets/js/application.js"></script>

<!-- End loading site level scripts -->

<script src="<?php echo base_url()?>assets/plugins/form-jasnyupload/fileinput.min.js"></script>               			<!-- File Input -->

<!--validation -->
<script src="<?= base_url()?>assets/jquery_validation_engine/jquery.validationEngine.js"></script>
<script src="<?= base_url()?>assets/jquery_validation_engine/jquery.validationEngine-en.js"></script>
<script src="<?= base_url()?>assets/custom.js"></script>
<script src="<?= base_url()?>assets/jquery.browser.js"></script>
<script src="<?= base_url()?>assets/jquery.form.js"></script>
<script src="<?= base_url()?>assets/toastr.js"></script>

<!--datatable -->
<script src="<?php echo base_url()?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url()?>assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url()?>assets/demo/demo-datatables.js"></script>

<!-- Summernote editor -->
<script src="<?php echo base_url()?>assets/plugins/summernote/dist/summernote.js"></script>  	<!-- Summernote -->

<script>
$(function () {
	$("#product_desc, .summernote").summernote({
		height: 200
	});
  $("#product_desc a, .summernote a").click(function(e){
      e.preventDefault();
  });
	$('.wmd-preview').addClass('well');
});
</script>
    </body>

</html>
