<table id="tbl-model" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>#</th>
            <th>License Key</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (isset($data)) {
            $count = 1;
            foreach ($data as $k => $v) {
                ?>
                <tr>
                    <td><?php echo $count; ?></td>
                    <td><?php echo $v['dealer_key']; ?></td>
                    <td><span class="badge badge-<?php echo ($v['status'] == 'active' ? "success" : "danger") ?>"><i class="fa fa-<?php echo ($v['status'] == "active" ? "check" : "times") ?>"></i> <?php echo $v['status']; ?></span></td>
                </tr>
                <?php
                $count++;
            }
        }
        ?>

    </tbody>
</table>
