 <div class="static-content-wrapper">
  <div class="static-content">
      <div class="page-content">
          <ol class="breadcrumb">


          </ol>
<div class="page-heading">
  <h1>Distributors</h1>
  <div class="options">
    <div class="btn-toolbar">
        <a href="#" class="btn btn-default"><i class="fa fa-fw fa-wrench"></i></a>
    </div>
  </div>
</div>
<div class="container-fluid">
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h2>Distributors</h2>

			</div>
			<div class="panel-body">
				<table id="example" class="table table-hover table-striped table-bordered dataTable" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>#</th>
              <th>Name</th>
  						<th>Contact</th>
  						<th>email</th>
              <th>Address</th>
              <th>Country</th>
              <th>City</th>
              <th>State</th>
              <th>Zip Code</th>

						</tr>
					</thead>
					<tbody>
            <?php
                if(isset($data)){
                    $count=1;
                    foreach( $data as $k=>$v ){
                        ?>
                        <tr>
                            <td><?php echo $count;?></td>
                            <td><a href="<?php echo site_url('distributors/licenseKeys')?>"><?php echo $v['fname']." ".$v['lname'];?></a></td>
                            <td><?php echo $v['contact'];?></td>
                            <td><?php echo $v['email'];?></td>
                            <td><?php echo $v['address'];?></td>
                            <td><?php echo $v['country'];?></td>
                            <td><?php echo $v['city'];?></td>
                            <td><?php echo $v['state'];?></td>
                            <td><?php echo $v['zip'];?></td>
                        </tr>
                    <?php
                    $count++;
                    }
                }
            ?>
					</tbody>
				</table>
				<div class="panel-footer"></div>
			</div>
		</div>
	</div>
</div>

</div> <!-- .container-fluid -->
</div> <!-- #page-content -->
</div>
