<!DOCTYPE html>
<html lang="en" class="coming-soon">
<head>
    <meta charset="utf-8">
    <title>Login Form</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="description" content="">
    <meta name="author" content="KaijuThemes">

    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url()?>assets/plugins/iCheck/skins/minimal/blue.css" type="text/css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/fonts/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/styles.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url()?>assets/toastr.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/jquery_validation_engine/template.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/jquery_validation_engine/validationEngine.jquery.css">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries. Placeholdr.js enables the placeholder attribute -->
    <!--[if lt IE 9]>
        <link href="assets/css/ie8.css" type="text/css" rel="stylesheet">
        <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The following CSS are included as plugins and can be removed if unused-->

    </head>

    <body class="focused-form">


<div class="container" id="login-form">
	<a href="index-2.html" class="login-logo"><img src="<?php echo base_url()?>/assets/img/logo.png" height="200"></a>
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<div class="panel panel-default">
				<div class="panel-heading"><h2>Login Form</h2></div>
				<div class="panel-body">

          <form method="post" action="<?php echo site_url('user/login')?>" class="form-horizontal ajaxForm nopopup">
						<div class="form-group">
              <div class="col-xs-12">
              	<div class="input-group">
									<span class="input-group-addon">
										<i class="fa fa-user"></i>
									</span>
                    <input type="text" class="form-control validate[required]" name="username" placeholder="Enter Name">
								</div>
              </div>
						</div>

						<div class="form-group">
              <div class="col-xs-12">
	              <div class="input-group">
									<span class="input-group-addon">
										<i class="fa fa-key"></i>
									</span>
                  <input type="password" name="password" class="form-control validate[required]" id="exampleInputPassword1" placeholder="Password">
								</div>
              </div>
						</div>

						<!-- <div class="form-group">
							<div class="col-xs-12">
								<a href="extras-forgotpassword.html" class="pull-left">Forgot password?</a>
								<div class="checkbox-inline icheck pull-right pt0">
									<label for="">
										<input type="checkbox"></input>
										Remember me
									</label>
								</div>
							</div>
						</div> -->


						<div class="panel-footer">
							<div class="clearfix">
								<!-- <a href="extras-registration.html" class="btn btn-default pull-left">Register</a> -->
                  <button type="submit" class="btn btn-primary pull-right">Login</button>
							</div>
						</div>
					</form>
				</div>
			</div>

			<!-- <div class="text-center">
				<a href="#" class="btn btn-label btn-social btn-facebook mb20"><i class="fa fa-fw fa-facebook"></i>Connect with Facebook</a>
				<a href="#" class="btn btn-label btn-social btn-twitter mb20"><i class="fa fa-fw fa-twitter"></i>Connect with Twitter</a>
			</div> -->
		</div>
	</div>
</div>



    <!-- Load site level scripts -->

<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script> -->

<script src="<?php echo base_url()?>assets/js/jquery-1.10.2.min.js"></script> 							<!-- Load jQuery -->
<script src="<?php echo base_url()?>assets/js/jqueryui-1.9.2.min.js"></script> 							<!-- Load jQueryUI -->
<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script> 								<!-- Load Bootstrap -->
<script src="<?php echo base_url()?>assets/plugins/easypiechart/jquery.easypiechart.js"></script> 		<!-- EasyPieChart-->
<script src="<?php echo base_url()?>assets/plugins/sparklines/jquery.sparklines.min.js"></script>  		<!-- Sparkline -->
<script src="<?php echo base_url()?>assets/plugins/jstree/dist/jstree.min.js"></script>  				<!-- jsTree -->
<script src="<?php echo base_url()?>assets/plugins/codeprettifier/prettify.js"></script> 				<!-- Code Prettifier  -->
<script src="<?php echo base_url()?>assets/plugins/bootstrap-switch/bootstrap-switch.js"></script> 		<!-- Swith/Toggle Button -->
<script src="<?php echo base_url()?>assets/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js"></script>  <!-- Bootstrap Tabdrop -->
<script src="<?php echo base_url()?>assets/plugins/iCheck/icheck.min.js"></script>     					<!-- iCheck -->
<script src="<?php echo base_url()?>assets/js/enquire.min.js"></script> 									<!-- Enquire for Responsiveness -->
<script src="<?php echo base_url()?>assets/plugins/bootbox/bootbox.js"></script>							<!-- Bootbox -->
<script src="<?php echo base_url()?>assets/plugins/simpleWeather/jquery.simpleWeather.min.js"></script> <!-- Weather plugin-->
<script src="<?php echo base_url()?>assets/plugins/nanoScroller/js/jquery.nanoscroller.min.js"></script> <!-- nano scroller -->
<script src="<?php echo base_url()?>assets/plugins/jquery-mousewheel/jquery.mousewheel.min.js"></script> 	<!-- Mousewheel support needed for jScrollPane -->
<script src="<?php echo base_url()?>assets/js/application.js"></script>
<script src="<?php echo base_url()?>assets/demo/demo.js"></script>
<script src="<?php echo base_url()?>assets/demo/demo-switcher.js"></script>
<script src="<?= base_url()?>assets/jquery_validation_engine/jquery.validationEngine.js"></script>
<script src="<?= base_url()?>assets/jquery_validation_engine/jquery.validationEngine-en.js"></script>
<script src="<?= base_url()?>assets/custom.js"></script>
<script src="<?= base_url()?>assets/jquery.browser.js"></script>
<script src="<?= base_url()?>assets/jquery.form.js"></script>
<script src="<?= base_url()?>assets/toastr.js"></script>
<!-- End loading site level scripts -->
    <!-- Load page level scripts-->
    <!-- End loading page level scripts-->
    </body>
</html>
