 <div class="static-content-wrapper">
                    <div class="static-content">
                        <div class="page-content">
                            <ol class="breadcrumb">


                            </ol>
                            <div class="page-heading">
                                <h1>Products</h1>
                                <div class="options">
    <div class="btn-toolbar">
        <a href="#" class="btn btn-default"><i class="fa fa-fw fa-wrench"></i></a>
    </div>
</div>
                            </div>
                            <div class="container-fluid">



<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h2>Products</h2>

			</div>
			<div class="panel-body">
				<table id="example" class="table table-hover table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>#</th>
              <th>Image</th>
							<th>Product Name</th>
							<th>Description</th>
              <th width="200">Action</th>
						</tr>
					</thead>
					<tbody>
            <?php
                if(isset($data)){
                    $count=1;
                    foreach( $data as $k=>$v ){
                        ?>
                        <tr>
                            <td><?php echo $count;?></td>
                            <td><img src="<?php echo base_url()."uploads/".$v['image'];?>" width="50" height="50" /></td>
                            <td><?php echo $v['title'];?></td>
                            <td><?php echo $v['desc'];?></td>
                            <td class="text-center">
                              <a href="<?php echo site_url('products/edit?q='.$v['id'])?>" class="btn-warning btn"><i class="fa fa-pencil"></i> Edit</a>
                              <a href="<?php echo site_url('products/delete/'.$v['id'])?>" rel="delete" class="ajax btn-danger btn"><i class="fa fa-trash"></i> Delete</a>
                            </td>
                        </tr>
                   <?php
                   $count++;
                    }
                }
            ?>
					</tbody>
				</table>
				<div class="panel-footer"></div>
			</div>
		</div>
	</div>
</div>

                            </div> <!-- .container-fluid -->
                        </div> <!-- #page-content -->
                    </div>
