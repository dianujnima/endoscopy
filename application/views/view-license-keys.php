 <div class="static-content-wrapper">
                    <div class="static-content">
                        <div class="page-content">
                            <ol class="breadcrumb">



                            </ol>
                            <div class="page-heading">
                                <h1>License Keys</h1>
                                <div class="options">
    <div class="btn-toolbar">
        <a href="#" class="btn btn-default"><i class="fa fa-fw fa-wrench"></i></a>
    </div>
</div>
                            </div>
                            <div class="container-fluid">



<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h2>License Keys</h2>

			</div>
			<div class="panel-body">
				<table id="example" class="table table-hover table-striped table-bordered dataTable" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>#</th>
              <th>Distributor</th>
              <th>Total License Keys</th>
              <th>Active Keys</th>
							<th>Used Keys</th>
              <th width="100">Action</th>
						</tr>
					</thead>
					<tbody>
            <?php
                if(isset($data)){
                    $count=1;
                    foreach( $data as $k=>$v ){
                        ?>
                        <tr>
                            <td><?php echo $count;?></td>
                            <td><?php echo $v['name'];?></td>
                            <td><?php echo $v['total'];?></td>
                            <td><?php echo $v['active'];?></td>
                            <td><?php echo $v['deactive'];?></td>
                            <td class="text-center"><a data-toggle="modal" id="<?php echo $v['id']?>" href="#myModal" class="btn btn-primary btn-small view-btn"><i class="fa fa-eye"></i> View</a></td>
                        </tr>
                    <?php
                    $count++;
                    }
                }
            ?>
					</tbody>
				</table>
				<div class="panel-footer"></div>
			</div>
		</div>
	</div>
</div>

                            </div> <!-- .container-fluid -->
                        </div> <!-- #page-content -->
                    </div>


    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-
         1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h2 class="modal-title">License Keys</h2>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->



    <script src="<?php echo base_url()?>assets/js/jquery-1.10.2.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.view-btn').click(function(e){
            var id = $(this).attr('id');
            e.preventDefault();
                $.ajax({
                    url:'<?php echo site_url('distributors/getSpecificLicenseKeys')?>',
                    type:'POST',
                    data:{dealer_id:id},
                    success:function(res){
                            $("#myModal .modal-body").html(res);
                                $("#tbl-model").dataTable();

                            $("#myModal").on('hidden.bs.modal', function(){
                                $("#tbl-model").remove();
                            });
                    }
                });
            });
        });



    </script>
