<div id="wrapper">
            <div id="layout-static">
                <div class="static-sidebar-wrapper sidebar-midnightblue">
                    <div class="static-sidebar">
                        <div class="sidebar">
                          <div class="widget stay-on-collapse text-center">
                              <img src="<?php echo base_url() ?>/assets/img/logo.png" width="200" class="navbar-logo"/>
                          </div>
                        	<div class="widget stay-on-collapse" id="widget-sidebar">
                              <nav role="navigation" class="widget-body">
                              	<ul class="acc-menu">
                              		<li class="nav-separator">NAVIGATION</li>
                                  <!-- <li><a href="<?php echo site_url('dashboard');?>"><i class="fa fa-home"></i><span>Dashboard</span></a></li> -->
                                  <li><a href="javascript:;"><i class="fa fa-shopping-cart"></i><span>Store</span></a>
                              			<ul class="acc-menu">
                              				<li><a href="<?php echo site_url('products/add')?>">Add Products</a></li>
                              				<li><a href="<?php echo site_url('products')?>">Products</a></li>
                              			</ul>
                              		</li>
                                  <li><a href="javascript:;"><i class="fa fa-car"></i><span>Distributor</span></a>
                              			<ul class="acc-menu">
                              				<li><a href="<?php echo site_url('distributors/add')?>">Add Distributor</a></li>
                              				<li><a href="<?php echo site_url('distributors')?>">View Distributors</a></li>
                              				<li><a href="<?php echo site_url('distributors/generateLicenseKeys')?>">Assign License Keys</a></li>
                                      <li><a href="<?php echo site_url('distributors/licenseKeys')?>">All License Keys</a></li>
                              			</ul>
                              		</li>

                              	</ul>
                              </nav>
                            </div>
                          </div>
                    </div>
                </div>
