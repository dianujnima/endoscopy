<div class="static-content-wrapper">
                    <div class="static-content">
                        <div class="page-content">
                            <ol class="breadcrumb">
                                

                            </ol>
                            <div class="page-heading">            
                                <h1>Assign License Keys</h1>
                                <div class="options">
    <div class="btn-toolbar">
        <a href="#" class="btn btn-default"><i class="fa fa-fw fa-wrench"></i></a>
    </div>
</div>
                            </div>
                            <div class="container-fluid">
                                

<div data-widget-group="group1">

	<div class="panel panel-default" data-widget='{"draggable": "false"}'>
		<div class="panel-heading">
			<h2>Assign License Keys</h2>
				<div class="panel-ctrls"
					data-actions-container="" 
					data-action-collapse='{"target": ".panel-body"}'
					data-action-expand=''
					data-action-colorpicker=''>
				</div>
		</div>
		<div class="panel-editbox" data-widget-controls=""></div>
		<div class="panel-body">
                    <form action="<?php echo site_url('distributors/generateLicenseKeys')?>" method="post" class="form-horizontal row-border ajaxForm">
				
				<div class="form-group">
					<label class="col-sm-2 control-label">Select Distributor</label>
					<div class="col-sm-8">
						<select class="form-control validate[required]" name="dealer_id">
                                                    <option value="">Select Distributor</option>
                                                    <?php 
                                                            if( isset($data) ){
                                                                foreach( $data as $k=>$v ){
                                                                    ?>
                                                                    <option value="<?php echo $v['id']?>"><?php echo $v['fname']." ".$v['lname']?></option>
                                                                <?php
                                                                
                                                                }
                                                            }
                                                        ?>
						</select>
					</div>
				</div>
                                <div class="form-group">
					<label class="col-sm-2 control-label">Total Number of License Keys</label>
					<div class="col-sm-8">
                                            <input type="text" name="total" class="form-control validate[required]">
					</div>
				</div>
                               
                                <div class="panel-footer">
                                    <div class="row">
                                            <div class="col-sm-8 col-sm-offset-2">
                                                <button type="submit" class="btn-primary btn">Submit</button>
                                            </div>
                                    </div>
                                </div>
			</form>
			
		</div>
	</div>
      
	

</div>


                            </div> <!-- .container-fluid -->
                        </div> <!-- #page-content -->
                    </div>
                    