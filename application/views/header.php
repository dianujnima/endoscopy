<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Gold Star Medical</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="description" content="Avenger Admin Theme">
    <meta name="author" content="KaijuThemes">

    <link href='http://fonts.googleapis.com/css?family=RobotoDraft:300,400,400italic,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,600,700' rel='stylesheet' type='text/css'>

    <!--[if lt IE 10]>
        <script src="assets/js/media.match.min.js"></script>
        <script src="assets/js/placeholder.min.js"></script>
    <![endif]-->

    <link href="<?php echo base_url()?>assets/fonts/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet">        <!-- Font Awesome -->
    <link href="<?php echo base_url()?>assets/css/styles.css" type="text/css" rel="stylesheet">                                     <!-- Core CSS with all styles -->

    <link href="<?php echo base_url()?>assets/plugins/jstree/dist/themes/avenger/style.min.css" type="text/css" rel="stylesheet">    <!-- jsTree -->
    <link href="<?php echo base_url()?>assets/plugins/codeprettifier/prettify.css" type="text/css" rel="stylesheet">                <!-- Code Prettifier -->
    <link href="<?php echo base_url()?>assets/plugins/iCheck/skins/minimal/blue.css" type="text/css" rel="stylesheet">              <!-- iCheck -->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries. Placeholdr.js enables the placeholder attribute -->
    <!--[if lt IE 9]>
        <link href="assets/css/ie8.css" type="text/css" rel="stylesheet">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js"></script>
        <script type="text/javascript" src="assets/plugins/charts-flot/excanvas.min.js"></script>
        <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The following CSS are included as plugins and can be removed if unused-->

<link href="<?php echo base_url()?>assets/plugins/form-select2/select2.css" type="text/css" rel="stylesheet">                        <!-- Select2 -->
<link href="<?php echo base_url()?>assets/plugins/form-multiselect/css/multi-select.css" type="text/css" rel="stylesheet">           <!-- Multiselect -->
<link href="<?php echo base_url()?>assets/plugins/form-fseditor/fseditor.css" type="text/css" rel="stylesheet">                      <!-- FullScreen Editor -->
<link href="<?php echo base_url()?>assets/plugins/form-tokenfield/bootstrap-tokenfield.css" type="text/css" rel="stylesheet">        <!-- Tokenfield -->
<link href="<?php echo base_url()?>assets/plugins/switchery/switchery.css" type="text/css" rel="stylesheet">        					<!-- Switchery -->

<link href="<?php echo base_url()?>assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" type="text/css" rel="stylesheet"> <!-- Touchspin -->

<link href="<?php echo base_url()?>assets/js/jqueryui.css" type="text/css" rel="stylesheet">        									<!-- jQuery UI CSS -->

<link href="<?php echo base_url()?>assets/plugins/iCheck/skins/minimal/_all.css" type="text/css" rel="stylesheet">                   <!-- Custom Checkboxes / iCheck -->
<link href="<?php echo base_url()?>assets/plugins/iCheck/skins/flat/_all.css" type="text/css" rel="stylesheet">
<link href="<?php echo base_url()?>assets/plugins/iCheck/skins/square/_all.css" type="text/css" rel="stylesheet">

<link href="<?php echo base_url()?>assets/plugins/card/lib/css/card.css" type="text/css" rel="stylesheet">

 <link rel="stylesheet" href="<?= base_url()?>assets/toastr.css">
<link rel="stylesheet" href="<?= base_url()?>assets/jquery_validation_engine/template.css">
<link rel="stylesheet" href="<?= base_url()?>assets/jquery_validation_engine/validationEngine.jquery.css">

<link href="<?php echo base_url()?>assets/plugins/datatables/dataTables.bootstrap.css" type="text/css" rel="stylesheet">
<link href="<?php echo base_url()?>assets/plugins/datatables/dataTables.fontAwesome.css" type="text/css" rel="stylesheet">
<link href="<?php echo base_url()?>assets/plugins/summernote/dist/summernote.css" type="text/css" rel="stylesheet">
    </head>

    <body class="infobar-offcanvas">

        <div id="headerbar">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-6 col-sm-2">
				<a href="#" class="shortcut-tile tile-brown">
					<div class="tile-body">
						<div class="pull-left"><i class="fa fa-pencil"></i></div>
					</div>
					<div class="tile-footer">
						Create Post
					</div>
				</a>
			</div>
			<div class="col-xs-6 col-sm-2">
				<a href="#" class="shortcut-tile tile-grape">
					<div class="tile-body">
						<div class="pull-left"><i class="fa fa-group"></i></div>
						<div class="pull-right"><span class="badge">2</span></div>
					</div>
					<div class="tile-footer">
						Contacts
					</div>
				</a>
			</div>
			<div class="col-xs-6 col-sm-2">
				<a href="#" class="shortcut-tile tile-primary">
					<div class="tile-body">
						<div class="pull-left"><i class="fa fa-envelope-o"></i></div>
						<div class="pull-right"><span class="badge">10</span></div>
					</div>
					<div class="tile-footer">
						Messages
					</div>
				</a>
			</div>
			<div class="col-xs-6 col-sm-2">
				<a href="#" class="shortcut-tile tile-inverse">
					<div class="tile-body">
						<div class="pull-left"><i class="fa fa-camera"></i></div>
						<div class="pull-right"><span class="badge">3</span></div>
					</div>
					<div class="tile-footer">
						Gallery
					</div>
				</a>
			</div>

			<div class="col-xs-6 col-sm-2">
				<a href="#" class="shortcut-tile tile-midnightblue">
					<div class="tile-body">
						<div class="pull-left"><i class="fa fa-cog"></i></div>
					</div>
					<div class="tile-footer">
						Settings
					</div>
				</a>
			</div>
			<div class="col-xs-6 col-sm-2">
				<a href="#" class="shortcut-tile tile-orange">
					<div class="tile-body">
						<div class="pull-left"><i class="fa fa-wrench"></i></div>
					</div>
					<div class="tile-footer">
						Plugins
					</div>
				</a>
			</div>
		</div>
	</div>
</div>
        <header id="topnav" class="navbar navbar-midnightblue navbar-fixed-top clearfix" role="banner">

	<!-- <span id="trigger-sidebar" class="toolbar-trigger toolbar-icon-bg">
		<a data-toggle="tooltips" data-placement="right" title="Toggle Sidebar"><span class="icon-bg"><i class="fa fa-fw fa-bars"></i></span></a>
	</span> -->

	<a class="navbar-brand" href="<?php echo site_url('dashboard');?>">Endoscopy</a>


	<div class="yamm navbar-left navbar-collapse collapse in">
		<ul class="nav navbar-nav">
			<li><a href="<?php echo site_url('products/add')?>">Add Product</a></li>
			<li><a href="<?php echo site_url('distributors/add')?>">Add Distributor</a></li>

		</ul>
	</div>

	<ul class="nav navbar-nav toolbar pull-right" style="padding-right:25px;">
    <li class="toolbar-icon-bg hidden-xs" id="trigger-fullscreen">
        <a href="#" class="toggle-fullscreen"><span class="icon-bg"><i class="fa fa-fw fa-arrows-alt"></i></span></i></a>
    </li>
		<li class="dropdown toolbar-icon-bg">
			<a href="#" class="dropdown-toggle" data-toggle='dropdown'><span class="icon-bg"><i class="fa fa-fw fa-user"></i></span></a>
			<ul class="dropdown-menu userinfo arrow">
				<li class="divider"></li>
          <li><a href="<?php echo site_url('user/logout')?>"><span class="pull-left">Sign Out</span> <i class="pull-right fa fa-sign-out"></i></a></li>
			</ul>
		</li>
	</ul>

</header>
