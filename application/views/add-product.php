<div class="static-content-wrapper">
                    <div class="static-content">
                        <div class="page-content">
                            <ol class="breadcrumb">



                            </ol>
                            <div class="page-heading">
                                <h1>Add Product</h1>
                                <div class="options">
    <div class="btn-toolbar">
        <a href="#" class="btn btn-default"><i class="fa fa-fw fa-wrench"></i></a>
    </div>
</div>
                            </div>
                            <div class="container-fluid">


<div data-widget-group="group1">

	<div class="panel panel-default" data-widget='{"draggable": "false"}'>
		<div class="panel-heading">
			<h2>Add Product</h2>
				<div class="panel-ctrls"
					data-actions-container=""
					data-action-collapse='{"target": ".panel-body"}'
					data-action-expand=''
					data-action-colorpicker=''
				>
				</div>
		</div>
		<div class="panel-editbox" data-widget-controls=""></div>
		<div class="panel-body">
                    <form action="<?php echo site_url('products/add')?>" method="post" class="form-horizontal row-border ajaxForm">
				<div class="form-group">
					<label class="col-sm-2 control-label">Product Title</label>
					<div class="col-sm-8">
                                            <input type="text" name="title" class="form-control validate[required]">
					</div>
				</div>
                               <div class="form-group">
					<label class="col-sm-2 control-label">Contact</label>
					<div class="col-sm-8">
                                            <input type="text" name="contact" class="form-control validate[required]">
					</div>
				</div>
                                <div class="form-group">
					<label class="col-sm-2 control-label">Email</label>
					<div class="col-sm-8">
                                            <input type="text" name="email" class="form-control validate[required,custom[email]]">
					</div>
				</div>

                                <div class="form-group">
					<label class="col-sm-2 control-label">Product Image</label>
					<div class="col-sm-5">
						<div class="fileinput fileinput-new" style="width: 100%;" data-provides="fileinput">
							<div class="fileinput-preview thumbnail mb20" data-trigger="fileinput" style="width: 100%; height: 150px;"></div>
							<div>
								<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                                <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input class="validate[required]" type="file" name="img"></span>

							</div>
						</div>
					</div>

				</div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Product Description</label>
                                    <textarea rows="10" name="desc" class="col-sm-8 validate[required]"></textarea>
                                </div>

                                <div class="panel-footer">
                                    <div class="row">
                                            <div class="col-sm-8 col-sm-offset-2">
                                                <button type="submit" class="btn-primary btn">Submit</button>
                                            </div>
                                    </div>
                                </div>
			</form>

		</div>
	</div>



</div>
                            </div> <!-- .container-fluid -->
                        </div> <!-- #page-content -->
                    </div>
