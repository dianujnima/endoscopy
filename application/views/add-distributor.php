<div class="static-content-wrapper">
                    <div class="static-content">
                        <div class="page-content">
                            <ol class="breadcrumb">
                                

                            </ol>
                            <div class="page-heading">            
                                <h1>Add Dealer</h1>
                                <div class="options">
    <div class="btn-toolbar">
        <a href="#" class="btn btn-default"><i class="fa fa-fw fa-wrench"></i></a>
    </div>
</div>
                            </div>
                            <div class="container-fluid">
                                

<div data-widget-group="group1">

	<div class="panel panel-default" data-widget='{"draggable": "false"}'>
		<div class="panel-heading">
			<h2>Add Dealer</h2>
				<div class="panel-ctrls"
					data-actions-container="" 
					data-action-collapse='{"target": ".panel-body"}'
					data-action-expand=''
					data-action-colorpicker=''
				>
				</div>
		</div>
		<div class="panel-editbox" data-widget-controls=""></div>
		<div class="panel-body">
                    <form action="<?php echo site_url('distributors/add')?>" method="post" class="form-horizontal row-border ajaxForm">
				<div class="form-group">
					<label class="col-sm-2 control-label">First Name</label>
					<div class="col-sm-8">
                                            <input type="text" name="fname" class="form-control validate[required]">
					</div>
				</div>
                                <div class="form-group">
					<label class="col-sm-2 control-label">Last Name</label>
					<div class="col-sm-8">
                                            <input type="text" name="lname" class="form-control validate[required]">
					</div>
				</div>
                                <div class="form-group">
					<label class="col-sm-2 control-label">Username</label>
					<div class="col-sm-8">
                                            <input type="text" name="username" class="form-control validate[required]">
					</div>
				</div>
                                <div class="form-group">
					<label class="col-sm-2 control-label">Password</label>
					<div class="col-sm-8">
                                            <input type="password" name="password" class="form-control validate[required]">
					</div>
				</div>
                                
				<div class="form-group">
					<label class="col-sm-2 control-label">Contact</label>
					<div class="col-sm-8">
                                            <input type="text" name="contact" class="form-control validate[required]">
					</div>
				</div>
                                <div class="form-group">
					<label class="col-sm-2 control-label">email</label>
					<div class="col-sm-8">
                                            <input type="text" name="email" class="form-control validate[required,custom[email]]">
					</div>
				</div>
                                <div class="form-group">
					<label class="col-sm-2 control-label">Address</label>
					<div class="col-sm-8">
                                            <input type="text" name="address" class="form-control validate[required]">
					</div>
				</div>
                                
				<div class="form-group">
					<label class="col-sm-2 control-label">Select Country</label>
					<div class="col-sm-8">
						<select class="form-control" name="country" id="country">
							<?php 
                                                            echo $this->basic_model->selectSelect2("",$country,"country","id");
                                                        ?>
						</select>
					</div>
				</div>
                                <div class="form-group">
					<label class="col-sm-2 control-label">City</label>
					<div class="col-sm-8">
                                            <input type="text" name="city" class="form-control validate[required]">
					</div>
				</div>
                                <div class="form-group">
					<label class="col-sm-2 control-label">State</label>
					<div class="col-sm-8">
                                            <input type="text" name="state" class="form-control validate[required]">
					</div>
				</div>  
                                <div class="form-group">
					<label class="col-sm-2 control-label">Zip Code</label>
					<div class="col-sm-8">
                                            <input type="text" name="zip" class="form-control validate[required]">
					</div>
				</div>
                                <div class="form-group">
					<label class="col-sm-2 control-label">Select License Key Quantity</label>
					<div class="col-sm-8">
						<select class="form-control" name="total" id="license_key_total">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
						</select>
					</div>
				</div>
                                <div class="panel-footer">
                                    <div class="row">
                                            <div class="col-sm-8 col-sm-offset-2">
                                                <button type="submit" class="btn-primary btn">Submit</button>
                                            </div>
                                    </div>
                                </div>
			</form>
			
		</div>
	</div>
      
	

</div>


                            </div> <!-- .container-fluid -->
                        </div> <!-- #page-content -->
                    </div>
                    