var delay;
$(document).ready(function() {
    initComp();
});
function initComp
()
{

    $("form").validationEngine({promptPosition: "centerTop", scroll: false,opacity:0.50});
    $("form.ajaxForm").ajaxForm({
        dataType: "json",
        beforeSubmit: function() {
            $("#loader").show();
            faction = $("form.login").attr("action");
            $("input[type=submit]").attr("disabled",'disabled');
            if (faction == undefined)
            {
                if( ! $("form.ajaxForm").hasClass('nopopup') ){
                    r = confirm("Are you sure?");
                  if (!r){
                      $("input[type=submit]").removeAttr("disabled");
                      $("#loader").hide();
                      return false;
                  }
                }
            }
        },
        error: function()
        {
            $("input[type=submit]").removeAttr("disabled");
            $("#loader").hide();
            error("Error Accured.Invalid File Format.");
        },
        success: function(data) {
            $("#loader").hide();
            if (data == null || data == "")
            {
                window.location.reload(true);
            }
            else if (data['error'] !== undefined)
            {
                error(data['error']);
            }
            else if (data['success'] !== undefined)
            {
                success(data['success']);
            }
            if (data['redirect'] !== undefined)
            {
                window.location = data['redirect'];
            }
            if (data['reload'] !== undefined)
            {
                window.location.reload(true);
            }
//            resetForm();
        }
    });


    delay = function(ms, func) {
        return setTimeout(func, ms);
    };

    toastr.options = {
        positionClass: 'toast-bottom-left'
    };


    $(".add_more").click(function() {
        data = $("#add_more_trade").html();
        $("#trade_div").append(data);
    })
    $(".Removediv").click(function() {
        $(this).parent().remove();
    });
    $(".ajax").click(function() {
        $("#loader").show();
        href = $(this).attr("href");
        rel = $(this).attr("rel");
        ele=$(this);
        if (rel === "delete")
        {
            var r = confirm("Do you want to perform this action?")
            if (r === true)
            {
                $.ajax({
                    url: href,
                    dataType: "json",
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        $("#loader").hide();
                        error("Request not completed.Please try Again");
                    },
                    success: function(data) {
                        $("#loader").hide();

                        if (data == null || data == "")
                        {
                            window.location.reload(true);
                        }
                        else if (data['error'] !== undefined)
                        {
                            error(data['error']);
                        }
                        else if (data['success'] !== undefined)
                        {
                            success(data['success']);
                        }
                        else if (data['redirect'] !== undefined)
                        {
                            window.location = data['redirect'];
                        }
                        if (data['deleteRow'] !== undefined)
                        {
                            ele.closest("tr").remove();
                        }
                        if (data['reload'] !== undefined)
                        {
                          setTimeout(function(){
                            window.location.reload(true);
                          },1500);
                        }
                    }
                });
            }
            else{$("#loader").hide();}
        }
        else
        {
            alert(href);
            $.ajax({
                url: href,
                dataType: "json",
                error: function(jqXHR, textStatus, errorThrown)
                {
                    $("#loader").hide();
                    error("Request not completed.Please try Again");
                },
                success: function(data) {
                    $("#loader").hide();
                    if (data == null || data == "")
                    {
                        window.location.reload(true);
                    }
                    else if (data['error'] !== undefined)
                    {
                        error(data['error']);
                    }
                    else if (data['success'] !== undefined)
                    {
                        success(data['success']);
                    }
                    else if (data['redirect'] !== undefined)
                    {
                        window.location = data['redirect'];
                    }
                    if (data['deleteRow'] !== undefined)
                    {
                        $(this).closest("tr").remove();
                    }
                }
            });
        }
        return false;
    });
    $(".date").datepicker(
            {
                changeMonth: true,
                changeYear: true,
                dateFormat: "yy-mm-dd"
            });
    /*$('.time').timepicker({
     timeFormat: "hh:mm tt"
     });*/
    $(".ajaxselect").change(function(){
        surl=$(this).attr("data-url");
        target_id=$(this).attr("data-target");
        val=$(this).val();
        $.ajax({
            url:surl+"/"+val,
            success:function(data)
            {
                $("#"+target_id).html(data);
            }
        });
    });
    if($.fn.select2)
    {
    $(".select2").select2();
}
}

function setSelected(id, value)
{
    $("#" + id + " option").each(function() {
        val = $(this).val();
        if (value == val)
        {
            $(this).attr("selected", "selected");
        }

    });
}
function deleteP(url)
{
    var r = confirm("Would you like to delete?")
    if (r == true)
    {
        window.location = url;
    }
}

function error(message)
{
    delay(200, function() {
        return toastr.error(message, 'Error');
    });
}
function success(message)
{
    delay(200, function() {
        return toastr.success(message, 'Success');
    });
}

function Removediv(val)
{
    $(val).parent().parent().parent().remove();
}
function resetForm()
{
    $("form input[type=text]").val("");
    $("form input[type=password]").val("");
    $("form select").val("");
}
function ajaxRequest(href)
{
    $.ajax({
        url: href,
        dataType: "json",
        error: function(jqXHR, textStatus, errorThrown)
        {
            $("#loader").hide();
            error("Request not completed.Please try Again");
        },
        success: function(data) {
            $("#loader").hide();
            if (data == null || data == "")
            {
                window.location.reload(true);
            }
            else if (data['error'] !== undefined)
            {
                error(data['error']);
            }
            else if (data['success'] !== undefined)
            {
                success(data['success']);
            }
            else if (data['redirect'] !== undefined)
            {
                window.location = data['redirect'];
            }
            if (data['deleteRow'] !== undefined)
            {
                $(this).closest("tr").remove();
            }
        }
    });
}
